import { ComponentFixture, TestBed } from '@angular/core/testing';

import { VOccasionsComponent } from './v-occasions.component';

describe('VOccasionsComponent', () => {
  let component: VOccasionsComponent;
  let fixture: ComponentFixture<VOccasionsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ VOccasionsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(VOccasionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
