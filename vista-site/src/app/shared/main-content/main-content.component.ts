import { Component, OnInit ,ComponentFactoryResolver, ViewContainerRef} from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-main-content',
  templateUrl: './main-content.component.html',
  styleUrls: ['./main-content.component.scss']
})
export class MainContentComponent implements OnInit {
  isShown: boolean = true ; 
pageHide:boolean;
  constructor(
    private viewContainerRef: ViewContainerRef,
    private cfr: ComponentFactoryResolver,private router: Router
  ) {}

  ngOnInit(): void {
  }

  async getLazy1() { 
    this.viewContainerRef.clear();
    const { VOccasionsComponent } = await import('../v-occasions/v-occasions.component');
    this.viewContainerRef.createComponent(
      this.cfr.resolveComponentFactory(VOccasionsComponent)
    );
    this.isShown= false ;
  }

  async getLazy2() {
    this.viewContainerRef.clear();
    const { VOffsitesComponent } = await import('../v-offsites/v-offsites.component');
    this.viewContainerRef.createComponent(
      this.cfr.resolveComponentFactory(VOffsitesComponent)
    );
    this.isShown= false ;
  }
  

}
