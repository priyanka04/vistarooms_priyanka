import { ComponentFixture, TestBed } from '@angular/core/testing';

import { VOffsitesComponent } from './v-offsites.component';

describe('VOffsitesComponent', () => {
  let component: VOffsitesComponent;
  let fixture: ComponentFixture<VOffsitesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ VOffsitesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(VOffsitesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
